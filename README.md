Assignment 3
=============

A 2D game implemented using WebGL


##Objective

To get as far as possible in the game


##Controls


- `right arrow` : to move to right lane

- `left arrow` : to move to left lane

- `up arrow` : to jump

- `down arrow` : to activate grayscale for a couple of seconds



##Displays on Webpage

- Score
- Level (total 3)
- Distance covered
- Time left for different boosts (Jump/Fly)

##Features implemented

- A chaser drone that tries to catch the player. Comes by when player trips over a wooden log.
- Telephone Booths : when player collides, game over.
- Wooden log : trip player, if player hits two in a row quickly, then game over
- Jump Boost : looks like a boot, gives player higher jump, enough to jump over telephone booths
- Fly Boost : looks like an upward arrow, player flies safe from obstacles 
- Coins : each coin gives player a score of +5, the more coins the player collects, the better
- Scenery : a dark and creepy forest, with nightsky and a road with 2 tracks
- Lightening : walls(forest) flashes after a small interval continuously


## How to Run

- go to folder 'src'

- type 'firefox index.html'
