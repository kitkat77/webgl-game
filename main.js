var leftwall = new Array();
var rightwall = new Array();
var floor = new Array();
var sky = new Array();
var barriers = new Array();
var blocks = new Array();
var coins = new Array();
var boots = new Array();
var rockets = new Array();
var over = 1;


var player = 1;

var cubeRotation = 0.0;
var score = 0;
var distance = 0;
var level_threshold = [300,1000,2000];
var level = 1;
var running = true;

var coin_ticks = 100;
var gray_ticks = 0;
var wall_gray_ticks = 100;
var over_ticks = 200;


main();

//
// Start here
//
function main() {
  const canvas = document.querySelector('#glcanvas');
  const gl = canvas.getContext('webgl') || canvas.getContext('experimental-webgl');

  // If we don't have a GL context, give up now

  if (!gl) {
    alert('Unable to initialize WebGL. Your browser or machine may not support it.');
    return;
  }

  // Vertex shader program

  const vsSource = `
    attribute vec4 aVertexPosition;
    attribute vec2 aTextureCoord;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    varying highp vec2 vTextureCoord;

    void main(void) {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      vTextureCoord = aTextureCoord;
    }
  `;

  // Fragment shader program

  const fsSourceGray = `
    varying highp vec2 vTextureCoord;
    uniform sampler2D uSampler;
    
    
    void main(void) {
      highp vec4 temp;
      temp = texture2D(uSampler, vTextureCoord);
      gl_FragColor = vec4(vec3(temp.r+temp.g+temp.b)/3.0,temp.a);
    }
  `;

  const fsSourceFlash = `
    varying highp vec2 vTextureCoord;
    uniform sampler2D uSampler;
    
    
    void main(void) {
      highp vec4 temp;
      temp = texture2D(uSampler, vTextureCoord);
      gl_FragColor = vec4(vec3(temp.r+temp.g+temp.b)/1.5,temp.a);
    }
  `;

  // Fragment shader program

  const fsSource = `
    varying highp vec2 vTextureCoord;
    uniform sampler2D uSampler;
    
    void main(void) {
      gl_FragColor = texture2D(uSampler, vTextureCoord);
    }
  `;


  // Initialize a shader program; this is where all the lighting
  // for the vertices and so forth is established.
  const shaderProgram = initShaderProgram(gl, vsSource, fsSource);
  const shaderProgramGray = initShaderProgram(gl, vsSource, fsSourceGray);
  const shaderProgramFlash = initShaderProgram(gl, vsSource, fsSourceFlash);

  // Collect all the info needed to use the shader program.
  // Look up which attributes our shader program is using
  // for aVertexPosition, aVevrtexColor and also
  // look up uniform locations.
  const programInfo = {
    program: shaderProgram,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
      textureCoord: gl.getAttribLocation(shaderProgram, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
      uSampler: gl.getUniformLocation(shaderProgram, 'uSampler'),
    },
  };

  const programInfoGray = {
    program: shaderProgramGray,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgramGray, 'aVertexPosition'),
      textureCoord: gl.getAttribLocation(shaderProgramGray, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgramGray, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgramGray, 'uModelViewMatrix'),
      uSampler: gl.getUniformLocation(shaderProgramGray, 'uSampler'),
    },
  };
  const programInfoFlash = {
    program: shaderProgramFlash,
    attribLocations: {
      vertexPosition: gl.getAttribLocation(shaderProgramFlash, 'aVertexPosition'),
      textureCoord: gl.getAttribLocation(shaderProgramFlash, 'aTextureCoord'),
    },
    uniformLocations: {
      projectionMatrix: gl.getUniformLocation(shaderProgramFlash, 'uProjectionMatrix'),
      modelViewMatrix: gl.getUniformLocation(shaderProgramFlash, 'uModelViewMatrix'),
      uSampler: gl.getUniformLocation(shaderProgramFlash, 'uSampler'),
    },
  };

  // Here's where we call the routine that builds all the
  // objects we'll be drawing.
 // const buffers = initBuffers(gl);

  var then = 0;
  player = new Cube([0.0,-1.5,-6.0],0.5,gl);
  chaser = new Chaser([0.0,1.0,-4.0],0.3,gl);
  chaser.step_length = player.step_length- 0.01;
  var width = 6.0;
  var dis = -6.0;
  for(let i=0; i<20; i++)
  {
    l = new Wall([-width,1.0,dis],gl);
    leftwall.push(l);
    r = new Wall([width,1.0,dis],gl);
    rightwall.push(r);
    f = new Floor([0,-5.0,dis],gl);
    floor.push(f);
    s = new Sky([0,7.0,dis],gl);
    sky.push(s);
    dis -= 6.0;

  }
  b1 = new Block([-1.5,-2.0,-40.0],gl);
  blocks.push(b1);
  b2 = new Block([1.5,-2.0,-80.0],gl);
  blocks.push(b2);
  b3 = new Block([1.5,-2.0,-120.0],gl);
  blocks.push(b3);
  b4 = new Block([-1.5,-2.0,-160.0],gl);
  blocks.push(b4);

  b1 = new Barrier([-1.5,-2.0,-60.0],gl);
  barriers.push(b1);
  b2 = new Barrier([-1.5,-2.0,-140.0],gl);
  barriers.push(b2);
   
  d = -20;
  r = 1;  
  for(let i=0; i<15; i++)
  {
    b = new Boot([r * 1.5,-1.5,d],gl);
    boots.push(b);
    d -= 500;
    r*=-1;
  }

  d = -77;
  r = -1;  
  for(let i=0; i<15; i++)
  {
    b = new Rocket([r * 1.5,-1.7,d],gl);
    rockets.push(b);
    //d -= 50;
    d -= 673;
    r*=-1;
  }
 over = new Over([0,0,0],gl);

 // Draw the scene repeatedly
  function render(now) {
    now *= 0.001;  // convert to seconds
    const deltaTime = now - then;
    then = now;
//    c = new Cube(gl);
//    cur_cube = [c];
//    cur_cube.push(c);
//    buffers = cur_cube[0].buffers;
    //buffers = Cube(gl);
    //buffers = initBuffers(gl);
    tick_elements(gl,deltaTime);
    tick_input(gl);
    if(!running && over_ticks==0)
      drawOver(gl, programInfo,deltaTime);
    else
      drawScene(gl, programInfo, programInfoGray,programInfoFlash,deltaTime);
    
    requestAnimationFrame(render);
  }
  requestAnimationFrame(render);
}

//
// Draw the scene.
//
function drawScene(gl, programInfo ,programInfoGray,programInfoFlash,deltaTime) {
  if(gray_ticks > 0)
    programInfo = programInfoGray;
  buffers = player.buffers;
  gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
  gl.clearDepth(1.0);                 // Clear everything
  gl.enable(gl.DEPTH_TEST);           // Enable depth testing
  gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

  // Clear the canvas before we start drawing on it.

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  // Create a perspective matrix, a special matrix that is
  // used to simulate the distortion of perspective in a camera.
  // Our field of view is 45 degrees, with a width/height
  // ratio that matches the display size of the canvas
  // and we only want to see objects between 0.1 units
  // and 100 units away from the camera.

  const fieldOfView = 45 * Math.PI / 180;   // in radians
  const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
  const zNear = 0.1;
  const zFar = 100.0;
  const projectionMatrix = mat4.create();

  // note: glmatrix.js always has the first argument
  // as the destination to receive the result.
  mat4.perspective(projectionMatrix,
                   fieldOfView,
                   aspect,
                   zNear,
                   zFar);

 const viewMatrix = mat4.create();
 eye = [player.position[0]/4,-1.5+2,player.position[2]+8];
 look = [player.position[0]/4,-1.5+1,player.position[2]];
 up = [0,1,0];
 mat4.lookAt(viewMatrix, eye, look, up);


/*  for(let i=0; i<cur_cube.length; i++)
  {
    cur_cube[i].draw(gl,programInfo);
  //  cur_cube[i].tick(deltaTime);
  }
  */
  player.draw(gl,programInfo,projectionMatrix,viewMatrix);
  chaser.draw(gl,programInfo,projectionMatrix,viewMatrix);
  var i;
  if(wall_gray_ticks<0)
    programInf = programInfoFlash;
  else
    programInf = programInfo;
  for(i=0; i<leftwall.length; i++)
  {
    leftwall[i].draw(gl,programInf,projectionMatrix,viewMatrix);
    rightwall[i].draw(gl,programInf,projectionMatrix,viewMatrix);

  }
  for(i=0; i<floor.length; i++)
  {
    floor[i].draw(gl,programInfo,projectionMatrix,viewMatrix);
  }
  for(i=0; i<sky.length; i++)
  {
    sky[i].draw(gl,programInfo,projectionMatrix,viewMatrix);
  }
  for(i=0; i<blocks.length; i++)
  {
    blocks[i].draw(gl,programInfo,projectionMatrix,viewMatrix);
  }
  for(i=0; i<barriers.length; i++)
  {
    barriers[i].draw(gl,programInfo,projectionMatrix,viewMatrix);
  }
  for(i=0; i<coins.length; i++)
  {
    coins[i].draw(gl,programInfo,projectionMatrix,viewMatrix);
  }
  for(i=0; i<boots.length; i++)
  {
    boots[i].draw(gl,programInfo,projectionMatrix,viewMatrix);
  }
  for(i=0; i<rockets.length; i++)
  {
    rockets[i].draw(gl,programInfo,projectionMatrix,viewMatrix);
  }
}

function drawOver(gl, programInfo,deltaTime) {
  buffers = player.buffers;
  gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
  gl.clearDepth(1.0);                 // Clear everything
  gl.enable(gl.DEPTH_TEST);           // Enable depth testing
  gl.depthFunc(gl.LEQUAL);            // Near things obscure far things

  // Clear the canvas before we start drawing on it.

  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

  // Create a perspective matrix, a special matrix that is
  // used to simulate the distortion of perspective in a camera.
  // Our field of view is 45 degrees, with a width/height
  // ratio that matches the display size of the canvas
  // and we only want to see objects between 0.1 units
  // and 100 units away from the camera.

  const fieldOfView = 45 * Math.PI / 180;   // in radians
  const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
  const zNear = 0.1;
  const zFar = 100.0;
  const projectionMatrix = mat4.create();

  // note: glmatrix.js always has the first argument
  // as the destination to receive the result.
  mat4.perspective(projectionMatrix,
                   fieldOfView,
                   aspect,
                   zNear,
                   zFar);

 const viewMatrix = mat4.create();
 eye = [0,0,8];
 look = [0,0,0];
 up = [0,1,0];
 mat4.lookAt(viewMatrix, eye, look, up);

if(over_ticks==0)
  over.draw(gl,programInfo,projectionMatrix,viewMatrix);

/*  for(let i=0; i<cur_cube.length; i++)
  {
    cur_cube[i].draw(gl,programInfo);
  //  cur_cube[i].tick(deltaTime);
  }
  */
}

function tick_elements(gl, deltaTime)
{
  if(!player.is_exist)
    running = false;

  if(!running && over_ticks>0)
    over_ticks--;

  distance += 0.1;
  detect_all_collisions();
  if(running)
  {
    player.tick(deltaTime);
    chaser.tick(deltaTime);
  }
  width = 6.0;
  dis = -6.0;
  if(leftwall[0].position[2] > player.position[2] + 10)
  {
    leftwall.splice(0,1);
    len = leftwall.length;
    l = new Wall([-width,1.0,dis+leftwall[len-1].position[2]],gl);
    leftwall.push(l);

  }
  if(rightwall[0].position[2] > player.position[2] + 10)
  {
    rightwall.splice(0,1);
    len = rightwall.length;
    l = new Wall([width,1.0,dis+rightwall[len-1].position[2]],gl);
    rightwall.push(l);

  }
  if(floor[0].position[2] > player.position[2] + 10)
  {
    floor.splice(0,1);
    len = floor.length;
    f = new Floor([0,floor[len-1].position[1],dis+floor[len-1].position[2]],gl);
    floor.push(f);
  }
  if(sky[0].position[2] > player.position[2] + 10)
  {
    sky.splice(0,1);
    len = sky.length;
    f = new Sky([0,sky[len-1].position[1],dis+sky[len-1].position[2]],gl);
    sky.push(f);
  }
  if(blocks.length > 0 && blocks[0].position[2] > player.position[2] + 5)
  {
    blocks.splice(0,1);
    len = blocks.length;
    r = Math.floor(Math.random() * 100);
    r = r%2;
    if(r==0)
      r=-1;
    b = new Block([r*1.5,-2,blocks[len-1].position[2]-40],gl);
    blocks.push(b);
  }
  if(barriers[0].position[2] > player.position[2] + 5)
  {
    barriers.splice(0,1);
    len = barriers.length;
    r = Math.floor(Math.random() * 100);
    r = r%2;
    if(r==0)
      r=-1;
    b = new Barrier([r*1.5,-2,barriers[len-1].position[2]-80],gl);
    barriers.push(b);
  }
  if(coin_ticks == 0)
  {
    coin_ticks = 60;
    r = Math.floor(Math.random() * 100);
    r = r%2;
    if(r==0)
      r=-1;
    c = new Coin([r*1.5,-1.5,player.position[2]-100],gl);
    coins.push(c);
  }
  if(coins.length > 0 && coins[0].position[2] > player.position[2] + 5)
  {
    coins.splice(0,1);
  }
  coin_ticks--;
  if(level<=3 && distance > level_threshold[level-1])
  {
    level++;
  }
  if(gray_ticks > 0)
  {
    gray_ticks--;
  }
  
  wall_gray_ticks--;
  if(wall_gray_ticks <= -20)
    wall_gray_ticks = 100;
  
//  if(!player.is_exist)
//    running = false;
  update_webpage();

}


function update_webpage()
{
  if(!running)
  {
    document.getElementById('jump-heading').innerHTML = "";
    document.getElementById('jump-ticks').innerHTML = "";
    document.getElementById('fly-heading').innerHTML = "";
    document.getElementById('fly-ticks').innerHTML = "";

    return;
  }
  document.getElementById('score').innerHTML = score;
  document.getElementById('distance').innerHTML = Math.floor(distance);
  document.getElementById('level').innerHTML = level;
  if(player.jump_boost_ticks > 0)
  {
    document.getElementById('jump-heading').innerHTML = "Jump Boost left";
    document.getElementById('jump-ticks').innerHTML = player.jump_boost_ticks;
  }
  else
  {
    document.getElementById('jump-heading').innerHTML = "-";
    document.getElementById('jump-ticks').innerHTML = "-";    
  }
  if(player.fly_boost_ticks > 0)
  {
    document.getElementById('fly-heading').innerHTML = "Fly Boost left";
    document.getElementById('fly-ticks').innerHTML = player.fly_boost_ticks;
  }
  else
  {
    document.getElementById('fly-heading').innerHTML = "-";
    document.getElementById('fly-ticks').innerHTML = "-";    
  }

}

function detect_all_collisions()
{
  for(let i=0; i<blocks.length; i++)
  {
    if(detect_collision(player,blocks[i]))
    {
      player.position[2] -= 2;
      player.trip();
      chaser.position[2] = player.position[2] + 2;
      break;
    }
  }
  for(let i=0; i<barriers.length; i++)
  {
    if(detect_collision(player,barriers[i]))
    {
      chaser.position[2] = player.position[2] + 2;
      player.is_exist = false;    
    }
  }
  for(let i=0; i<coins.length; i++)
  {
    if(detect_collision(player,coins[i]))
    {
      coins.splice(i,1);
      score += 5;
      break;
    }
  }
  for(let i=0; i<boots.length; i++)
  {
    if(detect_collision(player,boots[i]))
    {
      boots.splice(i,1);
      player.jump_boost();
      break;
    }
  }
  for(let i=0; i<rockets.length; i++)
  {
    if(detect_collision(player,rockets[i]))
    {
      rockets.splice(i,1);
      player.fly_boost();
      break;
    }
   }
}



function tick_input(gl)
{
  window.addEventListener("keypress",checkKeyPressed, false);
}


function checkKeyPressed(e)
{
  const left = 37, right = 39, up = 38,b_key = 66,down=40;
  if(e.keyCode == left && player.lane == 1)
  {
    player.lane = -1;
    player.speed_x = -1;
    chaser.lane = -1;
    chaser.speed_x = -0.7;
  }
  else if(e.keyCode == right && player.lane == -1)
  {
    //window.alert("detected");
    player.lane = 1;
    player.speed_x = 1;
    chaser.lane = 1;
    chaser.speed_x = 0.7;
  }
  if(e.keyCode == up)
  {
    console.log("space");
    player.jump();
  }
  if(e.keyCode == down)
  {
    console.log("b pressed");
    gray_ticks = 100;
  }
  
}

function detect_collision(a,b)
{
  a_x = a.position[0];
  a_y = a.position[1];
  a_z = a.position[2];
  b_x = b.position[0];
  b_y = b.position[1];
  b_z = b.position[2];
  a_dimx = a.scale * a.r_x;
  a_dimy = a.scale * a.r_y;
  a_dimz = a.scale * a.r_z;
  b_dimx = b.scale * b.r_x;
  b_dimy = b.scale * b.r_y;
  b_dimz = b.scale * b.r_z;


  if(Math.abs(a_x-b_x) < (a_dimx+b_dimx) && Math.abs(a_y-b_y) < (a_dimy+b_dimy) && Math.abs(a_z-b_z) < (a_dimz+b_dimz))
    return true;
  else
    return false;

}





//
// Initialize a shader program, so WebGL knows how to draw our data
//
function initShaderProgram(gl, vsSource, fsSource) {
  const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource);
  const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource);

  // Create the shader program

  const shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);

  // If creating the shader program failed, alert

  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    alert('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
    return null;
  }

  return shaderProgram;
}

//
// creates a shader of the given type, uploads the source and
// compiles it.
//
function loadShader(gl, type, source) {
  const shader = gl.createShader(type);

  // Send the source to the shader object

  gl.shaderSource(shader, source);

  // Compile the shader program

  gl.compileShader(shader);

  // See if it compiled successfully

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader));
    gl.deleteShader(shader);
    return null;
  }

  return shader;
}
